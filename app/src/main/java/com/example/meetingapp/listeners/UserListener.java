package com.example.meetingapp.listeners;

import com.example.meetingapp.models.User;

public interface UserListener {
    void initialVideoMeeting(User user);
    void initialAudioMeeting(User user);
    void onMultipleUsersAction(Boolean isMultipleUsersSelected);

}
