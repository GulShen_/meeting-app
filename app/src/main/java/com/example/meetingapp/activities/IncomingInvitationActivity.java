package com.example.meetingapp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.meetingapp.R;
import com.example.meetingapp.models.User;
import com.example.meetingapp.network.ApiClient;
import com.example.meetingapp.network.ApiService;
import com.example.meetingapp.utilities.Constants;

import org.jitsi.meet.sdk.JitsiMeetActivity;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URI;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IncomingInvitationActivity extends AppCompatActivity {

    @BindView(R.id.meeting_type)
    ImageView meetingType;
    @BindView(R.id.first_char_icon)
    TextView firstCharIcon;
    @BindView(R.id.first_name)
    TextView firstName;
    @BindView(R.id.email)
    TextView email;
    String meetingType_;
    private static final String TAG = "IncomingInvitationActiv";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");

        setContentView(R.layout.activity_incoming_invitation);
        ButterKnife.bind(this);

        meetingType_ = getIntent().getStringExtra(Constants.REMOTE_MSG_MEETING_TYPE);
        setData();
    }

    private void setData() {

        if (meetingType_ != null) {
            if (meetingType_.equals("video")) {
                meetingType.setImageResource(R.drawable.videocam);
            } else {
                meetingType.setImageResource(R.drawable.phone);
            }
        }
        String username = getIntent().getStringExtra(Constants.KEY_FIRST_NAME);
        firstCharIcon.setText(username.substring(0, 1));
        firstName.setText(username + " " + getIntent().getStringExtra(Constants.KEY_LAST_NAME));
        email.setText(getIntent().getStringExtra(Constants.KEY_EMAIL));
    }

    private void sendRemoteMessage(String remoteMessageBody, String type) {
        Log.i(TAG, "sendRemoteMessage: " + remoteMessageBody + "   " + type);
        ApiClient.getClient().create(ApiService.class).sendRemoteMessage(Constants.getRemoteMessageHeaders(), remoteMessageBody).enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                Log.i(TAG, "responce result: " + response.isSuccessful());
                Log.i(TAG, "responce result: " + response.message());
                Log.i(TAG, "responce result: " + response.body());
                Log.i(TAG, "responce result: " + response.code());

                if (response.isSuccessful()) {
                    if (type.equals(Constants.REMOTE_MSG_INVATATION_ACCEPTED)) {
                        Log.i(TAG, "onResponse: ");
                        try {
                            URL url=new URL("https://meet.jit.si");
                            JitsiMeetConferenceOptions.Builder builder = new JitsiMeetConferenceOptions.Builder();
                            builder.setServerURL(url);
                            builder.setRoom(getIntent().getStringExtra(Constants.REMOTE_MSG_MEETING_ROOM));
                            builder.setWelcomePageEnabled(false);
                            if (meetingType.equals("audio")){
                                builder.setVideoMuted(true);
                            }
                            JitsiMeetActivity.launch(IncomingInvitationActivity.this,builder.build());
                            finish();
                        }catch (Exception e){
                            Toast.makeText(IncomingInvitationActivity.this,""+response.message(),Toast.LENGTH_SHORT).show();
                        }
                        Toast.makeText(IncomingInvitationActivity.this, "Accepted", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i(TAG, "responce: "+response.body());
                        Toast.makeText(IncomingInvitationActivity.this, "Rejected", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(IncomingInvitationActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
                finish();

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.i(TAG, "onFailure: ");
                Toast.makeText(IncomingInvitationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void sendInvitationResponse(String type,String receiverToken){
        try {
            JSONArray tokens = new JSONArray();
            tokens.put(receiverToken);
            JSONObject body = new JSONObject();
            JSONObject data = new JSONObject();
            data.put(Constants.REMOTE_MSG_TYPE, Constants.REMOTE_MSG_INVATATION_RESPONSE);
            data.put(Constants.REMOTE_MSG_INVATATION_RESPONSE, type);

            body.put(Constants.REMOTE_MSG_Data, data);
            body.put(Constants.REMOTE_MSG_REQISTRATION_IDS, tokens);
            sendRemoteMessage(body.toString(), type);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            finish();
        }
        finish();
    }

    @OnClick({R.id.accept, R.id.decline})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.accept:
                sendInvitationResponse(Constants.REMOTE_MSG_INVATATION_ACCEPTED,getIntent().getStringExtra(Constants.REMOTE_MSG_INVITER_TOKEN));
                break;
            case R.id.decline:
                sendInvitationResponse(Constants.REMOTE_MSG_INVATATION_REJECTED,getIntent().getStringExtra(Constants.REMOTE_MSG_INVITER_TOKEN));
                break;
        }
    }

    private BroadcastReceiver invitationResponseReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra(Constants.REMOTE_MSG_INVATATION_RESPONSE);
            if (type != null) {
                if (type.equals(Constants.REMOTE_MSG_INVATATION_CANCELED)) {
                    Toast.makeText(context, "Invitation Canceled", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(invitationResponseReceiver, new IntentFilter(Constants.REMOTE_MSG_INVATATION_RESPONSE));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(invitationResponseReceiver);

    }
}