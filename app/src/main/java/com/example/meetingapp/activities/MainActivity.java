package com.example.meetingapp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.meetingapp.R;
import com.example.meetingapp.adapters.UserAdapter;
import com.example.meetingapp.listeners.UserListener;
import com.example.meetingapp.models.User;
import com.example.meetingapp.utilities.Constants;
import com.example.meetingapp.utilities.PreferenceManager;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements UserListener {

    PreferenceManager preferenceManager;

    @BindView(R.id.error_message)
    TextView errorMessage;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    private UserAdapter userAdapter;
    private List<User> users;
    ImageView imageConference;

    private int REQUEST_CODE_BATTERY_OPTIMIZATION = 1;


    @BindView(R.id.text_title)
    TextView textTitle;
    @BindView(R.id.sign_out_tv)
    TextView signOutTv;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        preferenceManager = new PreferenceManager(this);
        imageConference = findViewById(R.id.imageConference);
        textTitle.setText(preferenceManager.getString(Constants.KEY_FIRST_NAME) + " " + preferenceManager.getString(Constants.KEY_LAST_NAME));

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            Log.i(TAG, "onCreate: " + task.isSuccessful());//false
            if (task.isSuccessful() && task.getResult() != null) {
                sendFCMTokenToDatabase(task.getResult().getToken());
            }
        });

        RecyclerView userRecycler = findViewById(R.id.user_recycler);


        users = new ArrayList<>();
        userAdapter = new UserAdapter(users, this);
        userRecycler.setAdapter(userAdapter);
        swipeRefreshLayout.setOnRefreshListener(this::getUsers);
        getUsers();
        checkForNatteryOptimization();
    }

    private void getUsers() {
        swipeRefreshLayout.setRefreshing(true);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(Constants.KEY_COLLECTION_USERS)
                .get()
                .addOnCompleteListener(result -> {
                    swipeRefreshLayout.setRefreshing(false);
                    String myUserId = preferenceManager.getString(Constants.KEY_USER_ID);
                    if (result.isSuccessful() && result.getResult() != null) {
                        users.clear();
                        for (QueryDocumentSnapshot documentSnapshot : result.getResult()) {
                            if (myUserId.equals(documentSnapshot.getId())) {
                                continue;
                            }
                            User user = new User();
                            user.firstName = documentSnapshot.getString(Constants.KEY_FIRST_NAME);
                            user.lastName = documentSnapshot.getString(Constants.KEY_LAST_NAME);
                            user.email = documentSnapshot.getString(Constants.KEY_EMAIL);
                            user.token = documentSnapshot.getString(Constants.KEY_FCM_TOKEN);
                            users.add(user);
                        }

                        if (users.size() > 0) {
                            userAdapter.notifyDataSetChanged();
                        } else {
                            errorMessage.setText("No user Aviable");
                            errorMessage.setVisibility(View.VISIBLE);
                        }
                    } else {

                    }
                });
    }

    @OnClick(R.id.sign_out_tv)
    public void onViewClicked() {

        Toast.makeText(this, "Can", Toast.LENGTH_SHORT).show();

        signOut();
    }

    private void signOut() {
        try {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference documentReference = db.collection(Constants.KEY_COLLECTION_USERS)
                    .document(preferenceManager.getString(Constants.KEY_USER_ID));
            HashMap<String, Object> updates = new HashMap<>();
            updates.put(Constants.KEY_FCM_TOKEN, FieldValue.delete());
            documentReference.update(updates)
                    .addOnCompleteListener(success -> {
                        preferenceManager.clearPreferences();
                        Toast.makeText(this, "log out", Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(this, SignInActivity.class));
                        finish();
                    })
                    .addOnFailureListener(fail -> {
                        Toast.makeText(this, "Can not log out", Toast.LENGTH_SHORT).show();
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendFCMTokenToDatabase(String token) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference documentReference = db.collection(Constants.KEY_COLLECTION_USERS).document(preferenceManager.getString(Constants.KEY_USER_ID));

        documentReference.update(Constants.KEY_FCM_TOKEN, token)
                .addOnSuccessListener(success -> {
                })
                .addOnFailureListener(fail -> {
                    Toast.makeText(this, "Unable to send Token:" + fail.getMessage(), Toast.LENGTH_SHORT).show();
                });
    }

    @Override
    public void initialVideoMeeting(User user) {
        if (user.token == null || user.token.trim().isEmpty()) {
            Toast.makeText(this, user.firstName + " " + user.lastName + " is not aviable for meeting", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(this, OutgoingInvitationActivity.class);
            intent.putExtra("user", user);
            intent.putExtra("type", "video");
            startActivity(intent);

        }
    }

    @Override
    public void initialAudioMeeting(User user) {
        if (user.token == null || user.token.trim().isEmpty()) {
            Toast.makeText(this, user.firstName + " " + user.lastName + " is not aviable for meeting", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(this, OutgoingInvitationActivity.class);
            intent.putExtra("user", user);
            intent.putExtra("type", "audio");
            startActivity(intent);
        }
    }

    public void onMultipleUsersAction(Boolean isMultipleUsersSelected) {
        if (isMultipleUsersSelected) {
            imageConference.setVisibility(View.VISIBLE);
            imageConference.setOnClickListener(v -> {
                Intent intent = new Intent(getApplicationContext(), OutgoingInvitationActivity.class);
                intent.putExtra("selectedUser", new Gson().toJson(userAdapter.getSelectedUsers()));
                intent.putExtra("type", "video");
                intent.putExtra("isMultiple", true);
                startActivity(intent);
            });
        } else {
            imageConference.setVisibility(View.GONE);
        }
    }


    private void checkForNatteryOptimization() {
        Log.i(TAG, "checkForNatteryOptimization1: ");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PowerManager powerManager= (PowerManager) getSystemService(POWER_SERVICE);
            Log.i(TAG, "checkForNatteryOptimization2: ");
            if(!powerManager.isIgnoringBatteryOptimizations(getPackageName())){
                Log.i(TAG, "checkForNatteryOptimization: 3");
                AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Warning");
                builder.setMessage("Please choose your app from Settings and enable calls from backgound.");
                builder.setPositiveButton("Disable", (dialog, which) -> {
                    Intent intent=new Intent(Settings.ACTION_MANAGE_ALL_APPLICATIONS_SETTINGS);
                    startActivityForResult(intent,REQUEST_CODE_BATTERY_OPTIMIZATION);
                });
                builder.setNegativeButton("Cancel", (dialog, which) -> {
                    dialog.dismiss();
                });
                builder.create().show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQUEST_CODE_BATTERY_OPTIMIZATION){
            Log.i(TAG, "onActivityResult: ");
            checkForNatteryOptimization();
        }
    }
}