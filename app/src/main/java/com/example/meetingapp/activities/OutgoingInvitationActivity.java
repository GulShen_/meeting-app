package com.example.meetingapp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.meetingapp.R;
import com.example.meetingapp.models.User;
import com.example.meetingapp.network.ApiClient;
import com.example.meetingapp.network.ApiService;
import com.example.meetingapp.utilities.Constants;
import com.example.meetingapp.utilities.PreferenceManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.jitsi.meet.sdk.JitsiMeetActivity;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OutgoingInvitationActivity extends AppCompatActivity {

    @BindView(R.id.meeting_type)
    ImageView meetingType;
    @BindView(R.id.first_char_icon)
    TextView firstCharIcon;
    @BindView(R.id.first_name)
    TextView firstName;
    @BindView(R.id.email)
    TextView email;

    private PreferenceManager preferenceManager;
    private String inviterToken = null;
    User user;
    String meetingType_;
    String meetingRoom = null;

    private static final String TAG = "OutInvitationActivee";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outgoing_invitation);
        ButterKnife.bind(this);
        user = (User) getIntent().getSerializableExtra("user");
        meetingType_ = getIntent().getStringExtra("type");

        setData();
        preferenceManager = new PreferenceManager(this);


        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            Log.i(TAG, "onCreate: task:" + task.getResult() + "  " + task.isSuccessful());
            if (task.isSuccessful() && task.getResult() != null) {
                inviterToken = task.getResult().getToken();

                if (meetingType != null) {
                    if (getIntent().getBooleanExtra("isMultiple", false)) {
                        Type type = new TypeToken<ArrayList<User>>() {
                        }.getType();
                        ArrayList<User> receivers = new Gson().fromJson(getIntent().getStringExtra("selectedUser"), type);
                        Log.i(TAG, "onCreate: receivers:" + receivers.get(0).firstName);
                        initiateMeeting(meetingType_, null, receivers);
                    }
                 else {
                    Log.i(TAG, "onCreate__:meetingtype is null ");

                    if (user != null) {
                        Log.i(TAG, "onCreate:meetingtype is null ");
                        initiateMeeting(meetingType_, user.token, null);
                    }
                }}
                Log.i(TAG, "onCreate: token:" + inviterToken);

            }
        });

    }

    private void setData() {
        if (meetingType_ != null) {
            if (meetingType_.equals("video")) {
                meetingType.setImageResource(R.drawable.videocam);
            } else {
                meetingType.setImageResource(R.drawable.phone);
            }
        }
        if (user != null) {
            firstCharIcon.setText(user.firstName.substring(0, 1));
            firstName.setText(user.firstName + " " + user.lastName);
            email.setText(user.email);

        }

    }

    @OnClick(R.id.decline)
    public void onViewClicked() {
        if (user != null) {
            cancelInvitation(user.token);
        }

        onBackPressed();
    }

    private void initiateMeeting(String meetingType, String receiverToken, ArrayList<User> users) {
        Log.i(TAG, "initiateMeeting: ");
        try {
            JSONArray tokens = new JSONArray();

            if (receiverToken != null) {
                tokens.put(receiverToken);
            }

            if (users != null && users.size() > 0) {
                StringBuilder userNames = new StringBuilder();
                for (User user : users) {
                    tokens.put(user.token);
                    userNames.append(user.firstName).append(" ").append(user.lastName).append("\n");
                }
                firstCharIcon.setVisibility(View.GONE);
                email.setVisibility(View.GONE);
                firstName.setText(userNames.toString());
            }

            JSONObject body = new JSONObject();
            JSONObject data = new JSONObject();
            data.put(Constants.REMOTE_MSG_TYPE, Constants.REMOTE_MSG_INVITATION);
            data.put(Constants.REMOTE_MSG_MEETING_TYPE, meetingType);
            data.put(Constants.KEY_FIRST_NAME, preferenceManager.getString(Constants.KEY_FIRST_NAME));
            data.put(Constants.KEY_LAST_NAME, preferenceManager.getString(Constants.KEY_LAST_NAME));
            data.put(Constants.KEY_EMAIL, preferenceManager.getString(Constants.KEY_EMAIL));
            Log.i(TAG, "initiateMeeting: " + inviterToken);
            data.put(Constants.REMOTE_MSG_INVITER_TOKEN, inviterToken);

            meetingRoom = preferenceManager.getString(Constants.KEY_USER_ID) + "_" +
                    UUID.randomUUID().toString().substring(0, 5);
            data.put(Constants.REMOTE_MSG_MEETING_ROOM, meetingRoom);
            body.put(Constants.REMOTE_MSG_Data, data);
            body.put(Constants.REMOTE_MSG_REQISTRATION_IDS, tokens);
            sendRemoteMessage(body.toString(), Constants.REMOTE_MSG_INVITATION);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void sendRemoteMessage(String remoteMessageBody, String type) {
        Log.i(TAG, "sendRemoteMessage: " + remoteMessageBody + "   " + type);
        ApiClient.getClient().create(ApiService.class).sendRemoteMessage(Constants.getRemoteMessageHeaders(), remoteMessageBody).enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                Log.i(TAG, "responce result: " + response.isSuccessful());
                Log.i(TAG, "responce result: " + response.message());
                Log.i(TAG, "responce result: " + response.body());
                Log.i(TAG, "responce result: " + response.code());

                if (response.isSuccessful()) {
                    if (type.equals(Constants.REMOTE_MSG_INVITATION)) {
                        Log.i(TAG, "onResponse: ");
                        Toast.makeText(OutgoingInvitationActivity.this, "INV SENT", Toast.LENGTH_SHORT).show();
                    } else if (type.equals(Constants.REMOTE_MSG_INVATATION_RESPONSE)) {
                        Toast.makeText(OutgoingInvitationActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Log.i(TAG, "responce: " + response.body());
                        Toast.makeText(OutgoingInvitationActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

            }
            

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.i(TAG, "onFailure: ");
                Toast.makeText(OutgoingInvitationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void cancelInvitation(String receiverToken) {
        try {
            JSONArray tokens = new JSONArray();
            tokens.put(receiverToken);
            JSONObject body = new JSONObject();
            JSONObject data = new JSONObject();
            data.put(Constants.REMOTE_MSG_TYPE, Constants.REMOTE_MSG_INVATATION_RESPONSE);
            data.put(Constants.REMOTE_MSG_INVATATION_RESPONSE, Constants.REMOTE_MSG_INVATATION_CANCELED);

            body.put(Constants.REMOTE_MSG_Data, data);
            body.put(Constants.REMOTE_MSG_REQISTRATION_IDS, tokens);
            sendRemoteMessage(body.toString(), Constants.REMOTE_MSG_INVATATION_RESPONSE);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            finish();
        }
        finish();
    }

    private BroadcastReceiver invitationResponseReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra(Constants.REMOTE_MSG_INVATATION_RESPONSE);
            if (type != null) {
                if (type.equals(Constants.REMOTE_MSG_INVATATION_ACCEPTED)) {
                    try {
                        Log.i(TAG, "onReceive: " + meetingRoom);
                        URL url = new URL("https://meet.jit.si");
                        JitsiMeetConferenceOptions.Builder builder = new JitsiMeetConferenceOptions.Builder();
                        builder.setServerURL(url);
                        builder.setRoom(meetingRoom);
                        builder.setWelcomePageEnabled(false);

                        if (meetingType.equals("audio")) {
                            builder.setVideoMuted(true);
                        }

                        JitsiMeetActivity.launch(OutgoingInvitationActivity.this, builder.build());
                        finish();
                    } catch (Exception e) {
                        Toast.makeText(context, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    Toast.makeText(context, "Invitation Accepted", Toast.LENGTH_SHORT).show();
                } else if (type.equals(Constants.REMOTE_MSG_INVATATION_REJECTED)) {
                    Toast.makeText(context, "Invitation Rejected", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(invitationResponseReceiver, new IntentFilter(Constants.REMOTE_MSG_INVATATION_RESPONSE));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(invitationResponseReceiver);
    }
}