package com.example.meetingapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.meetingapp.R;
import com.example.meetingapp.utilities.Constants;
import com.example.meetingapp.utilities.PreferenceManager;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends AppCompatActivity {
    private static final String TAG = "SignInActivity";
    @BindView(R.id.mail)
    EditText mail;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.signInBtn)
    MaterialButton signInBtn;
    @BindView(R.id.sign_up)
    TextView signUp;
    @BindView(R.id.progress_circular)
    ProgressBar progressCircular;
    PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        preferenceManager = new PreferenceManager(this);
        findViewById(R.id.sign_up).setOnClickListener(c -> startActivity(new Intent(getApplicationContext(), SignUpActivity.class)));

        if (preferenceManager.getBoolean(Constants.KEY_IS_SIGNED)) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    @OnClick({R.id.signInBtn, R.id.sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.signInBtn:
                checkInputFields();
                break;
            case R.id.sign_up:
                break;
        }
    }

    private void checkInputFields() {
        if (mail.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Enter email", Toast.LENGTH_SHORT).show();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(mail.getText().toString()).matches()) {
            Toast.makeText(this, "Enter validemail", Toast.LENGTH_SHORT).show();
        } else if (password.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Enter password", Toast.LENGTH_SHORT).show();
        } else {
            signIn();
        }
    }

    private void signIn() {
        signInBtn.setVisibility(View.INVISIBLE);
        progressCircular.setVisibility(View.VISIBLE);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(Constants.KEY_COLLECTION_USERS)
                .whereEqualTo(Constants.KEY_EMAIL, mail.getText().toString())
                .whereEqualTo(Constants.KEY_PASSWORD, password.getText().toString())
                .get()
                .addOnCompleteListener(task -> {

                    if (task.isSuccessful() && task.getResult() != null && task.getResult().getDocuments().size() > 0) {

                        DocumentSnapshot documentSnapshot = task.getResult().getDocuments().get(0);
                        Log.i(TAG, "signIn: " + task.getResult().getDocuments().get(0)+"  user id:"+documentSnapshot.getId());
                        preferenceManager.putBoolean(Constants.KEY_IS_SIGNED, true);
                        preferenceManager.putString(Constants.KEY_USER_ID, documentSnapshot.getId());
                        preferenceManager.putString(Constants.KEY_FIRST_NAME, documentSnapshot.getString(Constants.KEY_FIRST_NAME));
                        preferenceManager.putString(Constants.KEY_LAST_NAME, documentSnapshot.getString(Constants.KEY_LAST_NAME));
                        preferenceManager.putString(Constants.KEY_EMAIL, documentSnapshot.getString(Constants.KEY_EMAIL));
                        Intent intent = new Intent(this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {
                        progressCircular.setVisibility(View.INVISIBLE);
                        signInBtn.setVisibility(View.VISIBLE);
                        Toast.makeText(this, "Unable to sign in", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}