package com.example.meetingapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.meetingapp.R;
import com.example.meetingapp.utilities.Constants;
import com.example.meetingapp.utilities.PreferenceManager;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.first_name)
    EditText firstName;
    @BindView(R.id.last_name)
    EditText lastName;
    @BindView(R.id.mail)
    EditText mail;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.confirm_password)
    EditText confirmPassword;
    @BindView(R.id.signUpBtn)
    MaterialButton signUpBtn;
    @BindView(R.id.progress_circular)
    ProgressBar progressCircular;

    private PreferenceManager preferenceManager;

    private static final String TAG = "SignUpActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        findViewById(R.id.sign_in).setOnClickListener(c -> onBackPressed());
        findViewById(R.id.back).setOnClickListener(c -> onBackPressed());
        preferenceManager = new PreferenceManager(getApplicationContext());
    }

    @OnClick(R.id.signUpBtn)
    public void onViewClicked() {
        checkInputFields();
    }

    private void checkInputFields() {
        if (firstName.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Enter first name", Toast.LENGTH_SHORT).show();
        } else if (lastName.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Enter last name", Toast.LENGTH_SHORT).show();
        } else if (mail.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Enter email", Toast.LENGTH_SHORT).show();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(mail.getText().toString()).matches()) {
            Toast.makeText(this, "Enter validemail", Toast.LENGTH_SHORT).show();
        } else if (password.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Enter password", Toast.LENGTH_SHORT).show();
        } else if (confirmPassword.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "confirm your password", Toast.LENGTH_SHORT).show();
        } else if (!password.getText().toString().equals(confirmPassword.getText().toString())) {
            Toast.makeText(this, "Password and con firm ", Toast.LENGTH_SHORT).show();
        } else {
            signUp();
        }
    }

    private void signUp() {
        signUpBtn.setVisibility(View.INVISIBLE);

        progressCircular.setVisibility(View.VISIBLE);

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        HashMap<String, Object> user = new HashMap<>();
        user.put(Constants.KEY_FIRST_NAME, firstName.getText().toString());
        user.put(Constants.KEY_LAST_NAME, lastName.getText().toString());
        user.put(Constants.KEY_EMAIL, mail.getText().toString());
        user.put(Constants.KEY_PASSWORD, password.getText().toString());

        db.collection("users")
                .add(user)
                .addOnSuccessListener(success -> {
                    Log.i(TAG, "signUp: success");
                    preferenceManager.putBoolean(Constants.KEY_IS_SIGNED, true);
                    preferenceManager.putString(Constants.KEY_USER_ID,success.getId());
                    preferenceManager.putString(Constants.KEY_FIRST_NAME, firstName.getText().toString());
                    preferenceManager.putString(Constants.KEY_LAST_NAME, lastName.getText().toString());
                    preferenceManager.putString(Constants.KEY_EMAIL, mail.getText().toString());
                    preferenceManager.putString(Constants.KEY_PASSWORD, password.getText().toString());
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                })
                .addOnFailureListener(failure -> {
                    progressCircular.setVisibility(View.INVISIBLE);
                    signUpBtn.setVisibility(View.VISIBLE);
                    Toast.makeText(this, "Error:"+failure.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.i(TAG, "signUp: failure");
                });

    }
}