package com.example.meetingapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.meetingapp.R;
import com.example.meetingapp.listeners.UserListener;
import com.example.meetingapp.models.User;

import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    private List<User> users;
    private UserListener userListener;
    private List<User> selectedUsers;

    public UserAdapter(List<User> user, UserListener userListener) {
        this.users = user;
        this.userListener = userListener;
        selectedUsers = new ArrayList<>();
    }

    public List<User> getSelectedUsers(){
    return selectedUsers;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        holder.setDataUser(users.get(position));
    }

    @Override
    public int getItemCount() {
        return users.size();
    }


    class UserViewHolder extends RecyclerView.ViewHolder {

        TextView name, mail, circle;
        ImageView phone, video;
        ConstraintLayout userContainer;
        ImageView imageSelected;


        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.username);
            mail = itemView.findViewById(R.id.mail);
            circle = itemView.findViewById(R.id.circle_center);
            phone = itemView.findViewById(R.id.phone_call);
            video = itemView.findViewById(R.id.video_call);
            userContainer = itemView.findViewById(R.id.user_container);
            imageSelected = itemView.findViewById(R.id.selectedUser);
        }

        void setDataUser(User user) {
            circle.setText(user.firstName.substring(0, 1));
            name.setText(user.firstName + " " + user.lastName);
            mail.setText(user.email);
            phone.setOnClickListener(click -> {
                userListener.initialAudioMeeting(user);
            });
            video.setOnClickListener(click -> {
                userListener.initialVideoMeeting(user);
            });
            userContainer.setOnLongClickListener(v -> {
                 selectedUsers.add(user);
                 imageSelected.setVisibility(View.VISIBLE);
                 phone.setVisibility(View.GONE);
                 video.setVisibility(View.GONE);
                 userListener.onMultipleUsersAction(true);
                return true;
            });

            userContainer.setOnClickListener(v->{
                if (imageSelected.getVisibility()==View.VISIBLE){
                   selectedUsers.remove(user);
                   imageSelected.setVisibility(View.GONE);
                   phone.setVisibility(View.VISIBLE);
                   video.setVisibility(View.VISIBLE);
                    if (selectedUsers.size()==0){
                        userListener.onMultipleUsersAction(false);
                    }
                }else{
                    if (selectedUsers.size()>0){
                        selectedUsers.add(user);
                        imageSelected.setVisibility(View.VISIBLE);
                        phone.setVisibility(View.GONE);
                        video.setVisibility(View.GONE);
                    }
                }
            });
        }
    }
}
