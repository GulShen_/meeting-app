package com.example.meetingapp.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class PreferenceManager {
    private SharedPreferences sp;
    private static final String TAG = "PreferenceManager";
    public PreferenceManager(Context c) {
        sp = c.getSharedPreferences(Constants.KEY_PREFERENCE_NAME, Context.MODE_PRIVATE);
    }
    public void putBoolean(String key,Boolean value){
        SharedPreferences.Editor editor=sp.edit();
        editor.putBoolean(key,value);
        editor.apply();
    }
    public Boolean getBoolean(String key){
        Log.i(TAG, "getBoolean: "+key);
        return  sp.getBoolean(key,false);
    }

    public void putString(String key,String value){
        SharedPreferences.Editor editor=sp.edit();
        editor.putString(key,value);
        editor.apply();
    }
    public  String getString(String key){
        Log.i(TAG, "getString: "+key);
        return  sp.getString(key,null);
    }
    
    public  void  clearPreferences(){
        Log.i(TAG, "clearPreferences: ");
        SharedPreferences.Editor editor=sp.edit();
        editor.clear();
        editor.apply();
    }
}
